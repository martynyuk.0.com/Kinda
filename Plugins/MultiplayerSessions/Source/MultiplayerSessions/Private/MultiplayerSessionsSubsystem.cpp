// Copyright Project Pulse. All Rights Reserved.

#include "MultiplayerSessionsSubsystem.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "Online/OnlineSessionNames.h"

UMultiplayerSessionsSubsystem::UMultiplayerSessionsSubsystem() :
	CreateSessionCompleteDelegate(FOnCreateSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnCreateSessionComplete)),
	FindSessionsCompleteDelegate(FOnFindSessionsCompleteDelegate::CreateUObject(this, &ThisClass::OnFindSessionsComplete)),
	JoinSessionCompleteDelegate(FOnJoinSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnJoinSessionComplete)),
	StartSessionCompleteDelegate(FOnStartSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnStartSessionComplete)),
	DestroySessionCompleteDelegate(FOnDestroySessionCompleteDelegate::CreateUObject(this, &ThisClass::OnDestroySessionComplete))
{
	if(const IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get())
	{
		SessionInterface = Subsystem->GetSessionInterface();
	}
}

void UMultiplayerSessionsSubsystem::CreateSession(const int32 InNumPublicConnections, const bool bInAllowJoinInProgress, const EMatchType InMatchType, const FSoftObjectPath InLobbyLevelPath)
{
	if(SessionInterface.IsValid() == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::CreateSession - SessionInterface is not valid!"));

		CustomOnCreateSessionComplete.Broadcast(false);
		return;
	}

	LastNumPublicConnections = InNumPublicConnections;
	bLastAllowJoinInProgress = bInAllowJoinInProgress;
	LastMatchType = InMatchType;
	LastLobbyLevelPath = InLobbyLevelPath.GetLongPackageName();
	//bCreateSessionOnDestroy = true;

	// Destroy the session if it already exists.
	if(SessionInterface->GetNamedSession(NAME_GameSession))
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::CreateSession - Session already exists, it will be destroyed to create a new one."));
		DestroySession();
	}

	// Store the delegate in a FDelegateHandle, so we can later remove it from the delegate list.
	Handle_CreateSessionCompleteDelegate = SessionInterface->AddOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegate);

	LastSessionSettings = MakeShareable(new FOnlineSessionSettings());
	LastSessionSettings->bIsLANMatch = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL" ? true : false;
	LastSessionSettings->NumPublicConnections = InNumPublicConnections;
	LastSessionSettings->bAllowJoinInProgress = true;
	LastSessionSettings->bAllowJoinViaPresence = bInAllowJoinInProgress;
	LastSessionSettings->bShouldAdvertise = true;
	LastSessionSettings->bUsesPresence = true;
	LastSessionSettings->bUseLobbiesIfAvailable = true;
	LastSessionSettings->BuildUniqueId = 1;
	LastSessionSettings->Set(FName("MatchType"), StaticCast<int32>(InMatchType), EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

	const ULocalPlayer* LocalPlayer = GetWorld()->GetFirstLocalPlayerFromController();
	const bool bSuccessfullyCreatedSession = SessionInterface->CreateSession(*LocalPlayer->GetPreferredUniqueNetId(), NAME_GameSession, *LastSessionSettings);
	if(bSuccessfullyCreatedSession == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::CreateSession - Session wasn't created successfully, clearing OnCreateSessionComplete delegate handle."));

		SessionInterface->ClearOnCreateSessionCompleteDelegate_Handle(Handle_CreateSessionCompleteDelegate);
		CustomOnCreateSessionComplete.Broadcast(false);
	}
}

void UMultiplayerSessionsSubsystem::FindSessions(const int32 MaxSearchResults)
{
	if(SessionInterface.IsValid() == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::FindSessions - SessionInterface is not valid!"));

		CustomOnFindSessionsComplete.Broadcast({}, false);
		return;
	}

	Handle_FindSessionsCompleteDelegate = SessionInterface->AddOnFindSessionsCompleteDelegate_Handle(FindSessionsCompleteDelegate);

	LastSessionSearch = MakeShareable(new FOnlineSessionSearch());
	LastSessionSearch->MaxSearchResults = MaxSearchResults;
	LastSessionSearch->bIsLanQuery = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL" ? true : false;
	LastSessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

	const ULocalPlayer* LocalPlayer = GetWorld()->GetFirstLocalPlayerFromController();
	const bool bWasSuccessful = SessionInterface->FindSessions(*LocalPlayer->GetPreferredUniqueNetId(), LastSessionSearch.ToSharedRef());
	if(bWasSuccessful == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::FindSessions - Session wasn't found successfully, clearing OnFindSessionsComplete delegate handle."));

		SessionInterface->ClearOnFindSessionsCompleteDelegate_Handle(Handle_FindSessionsCompleteDelegate);
		CustomOnFindSessionsComplete.Broadcast({}, false);
	}
}

void UMultiplayerSessionsSubsystem::JoinSession(const FOnlineSessionSearchResult& SessionSearchResult)
{
	if(SessionInterface.IsValid() == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::JoinSession - SessionInterface is not valid!"));

		CustomOnJoinSessionComplete.Broadcast(EOnJoinSessionCompleteResult::UnknownError);
		return;
	}

	Handle_JoinSessionCompleteDelegate = SessionInterface->AddOnJoinSessionCompleteDelegate_Handle(JoinSessionCompleteDelegate);

	const ULocalPlayer* LocalPlayer = GetWorld()->GetFirstLocalPlayerFromController();
	const bool bWasSuccessful = SessionInterface->JoinSession(*LocalPlayer->GetPreferredUniqueNetId(), NAME_GameSession, SessionSearchResult);
	if(bWasSuccessful == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::JoinSession - Session wasn't joined successfully, clearing OnJoinSessionComplete delegate handle."));

		SessionInterface->ClearOnJoinSessionCompleteDelegate_Handle(Handle_JoinSessionCompleteDelegate);
		CustomOnJoinSessionComplete.Broadcast(EOnJoinSessionCompleteResult::UnknownError);
	}
}

void UMultiplayerSessionsSubsystem::StartSession()
{
	if(SessionInterface.IsValid() == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::StartSession - SessionInterface is not valid!"));

		CustomOnStartSessionComplete.Broadcast(false);
		return;
	}

	Handle_StartSessionCompleteDelegate = SessionInterface->AddOnStartSessionCompleteDelegate_Handle(StartSessionCompleteDelegate);

	const bool bWasSuccessful = SessionInterface->StartSession(NAME_GameSession);
	if(bWasSuccessful == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::StartSession - Session wasn't started successfully, clearing OnStartSessionComplete delegate handle."));

		SessionInterface->ClearOnStartSessionCompleteDelegate_Handle(Handle_StartSessionCompleteDelegate);
		CustomOnStartSessionComplete.Broadcast(false);
	}
}

void UMultiplayerSessionsSubsystem::DestroySession()
{
	if(SessionInterface.IsValid() == false)
	{
		UE_LOG(LogMultiplayerPlugin, Error, TEXT("UMultiplayerSessionsSubsystem::DestroySession - SessionInterface is not valid!"));

		CustomOnDestroySessionComplete.Broadcast(false);
		return;
	}

	Handle_DestroySessionCompleteDelegate = SessionInterface->AddOnDestroySessionCompleteDelegate_Handle(DestroySessionCompleteDelegate);

	const bool bWasSuccessful = SessionInterface->DestroySession(NAME_GameSession);
	if(bWasSuccessful == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::DestroySession - Session wasn't destroyed successfully, clearing OnDestroySessionComplete delegate handle."));

		SessionInterface->ClearOnDestroySessionCompleteDelegate_Handle(Handle_DestroySessionCompleteDelegate);
		CustomOnDestroySessionComplete.Broadcast(false);
	}
}

void UMultiplayerSessionsSubsystem::OnCreateSessionComplete(FName SessionName, bool bWasSuccessful)
{
	if(SessionInterface.IsValid() == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::OnCreateSessionComplete - SessionInterface is not valid!"));

		CustomOnCreateSessionComplete.Broadcast(false);
		return;
	}

	SessionInterface->ClearOnCreateSessionCompleteDelegate_Handle(Handle_CreateSessionCompleteDelegate);
	if(bWasSuccessful)
	{
		UE_LOG(LogMultiplayerPlugin, Log, TEXT("UMultiplayerSessionsSubsystem::OnCreateSessionComplete - Session successfully created, ServerTravel to the default Lobby called."));
		if(UWorld* World = GetWorld())
		{
			World->ServerTravel(LastLobbyLevelPath + "?listen");
		}
	}

	CustomOnCreateSessionComplete.Broadcast(bWasSuccessful);
}

void UMultiplayerSessionsSubsystem::OnFindSessionsComplete(bool bWasSuccessful)
{
	if(SessionInterface.IsValid() == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::OnFindSessionsComplete - SessionInterface is not valid!"));

		CustomOnFindSessionsComplete.Broadcast({}, false);
		return;
	}

	SessionInterface->ClearOnFindSessionsCompleteDelegate_Handle(Handle_FindSessionsCompleteDelegate);

	if(LastSessionSearch->SearchResults.IsEmpty())
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::OnFindSessionsComplete - SearchResult is an empty array, cannot join any session"));

		CustomOnFindSessionsComplete.Broadcast({}, false);
		return;
	}

	for(const auto& SearchResult : LastSessionSearch->SearchResults)
	{
		int32 SettingsValue = 0;
		SearchResult.Session.SessionSettings.Get(FName("MatchType"), SettingsValue);

		if(StaticCast<EMatchType>(SettingsValue) == LastMatchType)
		{
			JoinSession(SearchResult);
			return;
		}
	}

	CustomOnFindSessionsComplete.Broadcast(LastSessionSearch->SearchResults, bWasSuccessful);
}

void UMultiplayerSessionsSubsystem::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	if(SessionInterface.IsValid() == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::OnJoinSessionComplete - SessionInterface is not valid!"));

		CustomOnJoinSessionComplete.Broadcast(EOnJoinSessionCompleteResult::UnknownError);
		return;
	}

	SessionInterface->ClearOnJoinSessionCompleteDelegate_Handle(Handle_JoinSessionCompleteDelegate);

	// Getting the address of the found session to connect to it
	FString Address;
	SessionInterface->GetResolvedConnectString(NAME_GameSession, Address);

	if(APlayerController* PC = GetGameInstance()->GetFirstLocalPlayerController())
	{
		PC->ClientTravel(Address, TRAVEL_Absolute);
	}

	CustomOnJoinSessionComplete.Broadcast(Result);
}

void UMultiplayerSessionsSubsystem::OnStartSessionComplete(FName SessionName, bool bWasSuccessful)
{
	if(SessionInterface.IsValid() == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::OnStartSessionComplete - SessionInterface is not valid!"));

		CustomOnStartSessionComplete.Broadcast(false);
		return;
	}
	if(bWasSuccessful/* && bCreateSessionOnDestroy*/)
	{
		//bCreateSessionOnDestroy = false;
		CreateSession(LastNumPublicConnections, bLastAllowJoinInProgress, LastMatchType, LastLobbyLevelPath);
	}

	SessionInterface->ClearOnStartSessionCompleteDelegate_Handle(Handle_StartSessionCompleteDelegate);
	CustomOnStartSessionComplete.Broadcast(bWasSuccessful);
}

void UMultiplayerSessionsSubsystem::OnDestroySessionComplete(FName SessionName, bool bWasSuccessful)
{
	if(SessionInterface.IsValid() == false)
	{
		UE_LOG(LogMultiplayerPlugin, Warning, TEXT("UMultiplayerSessionsSubsystem::OnDestroySessionComplete - SessionInterface is not valid!"));

		CustomOnDestroySessionComplete.Broadcast(false);
		return;
	}

	SessionInterface->ClearOnDestroySessionCompleteDelegate_Handle(Handle_StartSessionCompleteDelegate);
	CustomOnDestroySessionComplete.Broadcast(bWasSuccessful);
}