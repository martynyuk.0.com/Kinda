﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Logging/LogCategory.h"

DECLARE_LOG_CATEGORY_EXTERN(LogMultiplayerPlugin, Log, All);

UENUM(BlueprintType)
enum class EMatchType : uint8
{
	TeamDeathmatch UMETA(DisplayName = "Team Deathmatch"),
	FlagCapture UMETA(DisplayName = "Flag Capture")
};
