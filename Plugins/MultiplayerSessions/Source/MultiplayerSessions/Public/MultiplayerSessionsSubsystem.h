// Copyright Project Pulse. All Rights Reserved.

#pragma once

#include "Interfaces/OnlineSessionInterface.h"
#include "MultiplayerSessions/MultiplayerPluginCores.h"

#include "MultiplayerSessionsSubsystem.generated.h"

/**
* Declaring our own custom delegates for the Menu class to bind callbacks to.
*/
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCustomOnCreateSessionComplete, bool, bWasSuccessful);
DECLARE_MULTICAST_DELEGATE_TwoParams(FCustomOnFindSessionsComplete, const TArray<FOnlineSessionSearchResult>& SessionSearchResults, bool bWasSuccessful);
DECLARE_MULTICAST_DELEGATE_OneParam(FCustomOnJoinSessionComplete, EOnJoinSessionCompleteResult::Type Result);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCustomOnStartSessionComplete, bool, bWasSuccessful);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCustomOnDestroySessionComplete, bool, bWasSuccessful);

UCLASS(Blueprintable)
class MULTIPLAYERSESSIONS_API UMultiplayerSessionsSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UMultiplayerSessionsSubsystem();

	// To handle session functionality. The Menu class will call these.
	/**
	 * Function that handles session creating.
	 * @param InNumPublicConnections - number of players that can join the session.
	 * @param bInAllowJoinInProgress - whether or not you're allowed to join the session if it is already in progress.
	 * @param InMatchType - type of the match to create the session.
	 * @param InLobbyLevelPath - path to the level that will be a temp lobby.
	 */
	UFUNCTION(BlueprintCallable)
	void CreateSession(
		const int32 InNumPublicConnections = 4,
		const bool bInAllowJoinInProgress = true,
		const EMatchType InMatchType = EMatchType::TeamDeathmatch,
		const FSoftObjectPath InLobbyLevelPath = FSoftObjectPath());

	/**
	 * Function that handles finding sessions.
	 * @param MaxSearchResults - number of sessions we want to look for.
	 */
	UFUNCTION(BlueprintCallable)
	void FindSessions(const int32 MaxSearchResults = 10000);

	/**
	 * Function that handles joining to the session.
	 * Once we've found available sessions in FindSessions we'll have SessionSearchResult.
	 * @param SessionSearchResult - will be passed from the result of FindSessions process.
	 */
	void JoinSession(const FOnlineSessionSearchResult& SessionSearchResult);

	UFUNCTION(BlueprintCallable)
	void StartSession();

	void DestroySession();

protected:
	// Internal callbacks for the delegates we'll add to the Online Session Interface delegate list.
	// These don't need to be called outside this class.
	void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);
	void OnFindSessionsComplete(bool bWasSuccessful);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
	void OnStartSessionComplete(FName SessionName, bool bWasSuccessful);
	void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);

private:
	IOnlineSessionPtr SessionInterface;

	// Setting that we used for the last created session.
	TSharedPtr<FOnlineSessionSettings> LastSessionSettings;

	TSharedPtr<FOnlineSessionSearch> LastSessionSearch;

	// To add to the Online Session Interface delegate list.
	// We'll bind our MultiplayerSessionSubsystem internal callbacks to these.
	FOnCreateSessionCompleteDelegate CreateSessionCompleteDelegate;
	FDelegateHandle Handle_CreateSessionCompleteDelegate;

	FOnFindSessionsCompleteDelegate FindSessionsCompleteDelegate;
	FDelegateHandle Handle_FindSessionsCompleteDelegate;

	FOnJoinSessionCompleteDelegate JoinSessionCompleteDelegate;
	FDelegateHandle Handle_JoinSessionCompleteDelegate;

	FOnStartSessionCompleteDelegate StartSessionCompleteDelegate;
	FDelegateHandle Handle_StartSessionCompleteDelegate;

	FOnDestroySessionCompleteDelegate DestroySessionCompleteDelegate;
	FDelegateHandle Handle_DestroySessionCompleteDelegate;

public:
	/**
	 * Out own custom delegates for the menu class to bind callbacks to.
	 */
	FCustomOnCreateSessionComplete CustomOnCreateSessionComplete;
	FCustomOnFindSessionsComplete CustomOnFindSessionsComplete;
	FCustomOnJoinSessionComplete CustomOnJoinSessionComplete;
	FCustomOnStartSessionComplete CustomOnStartSessionComplete;
	FCustomOnDestroySessionComplete CustomOnDestroySessionComplete;

private:
	// TODO: on GameMode in Logout(maybe) call multicast destroy session
	// bool bCreateSessionOnDestroy{ false };

	EMatchType LastMatchType { EMatchType::TeamDeathmatch };
	FString LastLobbyLevelPath{};
	int32 LastNumPublicConnections{};
	bool bLastAllowJoinInProgress{ true };

};
