// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KindaGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class KINDA_API AKindaGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
